const Sitemapper = require('sitemapper');
const { getRandom } = require('./helpers/random');

async function parseSitemap(url, limit=true, n=200) {
  let sitemap = new Sitemapper();
  let res = await sitemap.fetch(url);
  let sites = res.sites;

  if (limit) {
    sites = getRandom(sites, n);
  }
  return sites;
}

// parseSitemap('https://www.habitat.co.uk/media/sitemap.xml');

module.exports.parseSitemap = parseSitemap;