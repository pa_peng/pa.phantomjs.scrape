const puppeteer = require('puppeteer');
const _cliProgress = require('cli-progress');
const { retry } = require('./helpers/retry');
const { clientProductPages } = require('./clients/client_product_pages');
const { Page } = require('./page');

// https://www.habitat.co.uk/media/sitemap.xml

class TestPhantomTracker {
  constructor(productUrls, webisteName) {
    this.timeout = 10000;  //30000
    this.retryTimes = 1;
    this.exponentialBackoff = true;
    this.bar = new _cliProgress.SingleBar({}, _cliProgress.Presets.shades_classic);
    this.productUrls = productUrls;
    this.webisteName = webisteName
    this.output = {};
    this.statistics = {
      success: 0,
      failure: 0,
    };
  }

  async run () {
    this.bar.start(this.productUrls.length, 0);
    const browser = await puppeteer.launch({
      timeout: this.timeout
    });
    await this._start(browser);
    return this.output;
  }

  async _start(browser) {
    try {
      // we need something to store a list of JSON files.
      for (let i = 0; i < this.productUrls.length; i++) {
        await retry(
          async () => {
            // try {
              let page = await this._fetchNewPage(browser, this.productUrls[i]);
              this.output[this.productUrls[i]] = page.getJsonSummary();
              this.statistics.success++;
            // } catch(e) {
            //   this.statistics.failure++;
            //   console.error('Error - something went wrong waitForRequest1: ', e.message);
            // }
          },
          this.timeout,
          this.retryTimes,
          this.exponentialBackoff,
        ).catch((e) => {
          this.statistics.failure++;
          console.error('Error - something went wrong waitForRequest2: ', e.message);
        })
        this.bar.update(i+1);
      }
      this._stop(browser)
    } catch(e) {
      console.error('Error - something went wrong in start function: ', e.message);
    }
  }

  _stop(browser) {
    console.log('\n', 'Closing browser');
    console.log('Statistics: ', this.statistics);
    browser.close();
    this.bar.stop();
  }

  async _fetchNewPage(browser, url) {
    const page = await browser.newPage();
    const response = await page.goto(url);
    const pageObject = new Page(url);
    console.log('\n', '[PA Phantom Tracker] Visiting URL: ', url);

    let status = response.status();
    if (status !== 200) {
      console.log('[PA Phantom Tracker] Page loaded with code: ', status);
      pageObject.setStatus(status);
      pageObject.setMessage('Non 200 response code detected.');
      return pageObject;
    }

    const chain = response.request().redirectChain();
    if (chain.length > 0) {
      console.log('[PA Phantom Tracker] Page triggers a redirect');
      pageObject.setStatus('300');
      pageObject.setMessage('Redirect Detected.');
      return pageObject;
    }

    let productPage = await this._checkProductPage(page);
    console.log('[PA Phantom Tracker] This is ', productPage ? 'a product page' : 'not a product page');
    if (productPage) {
      await page.waitForRequest((r) => {
        //https://api.particularaudience.com/2.2/UpdateProductDetails
        if(r.url() === 'https://api.particularaudience.com/2.2/Sync') {
          console.log('[PA Phantom Tracker] reuest url: ', r.url())
          console.log('PPP I am here!!!!!')
          let payload = JSON.parse(r.postData());
          console.log('PPP the payload', payload)
          if(payload && payload.e && payload.e.length > 0 && payload.e[0] && payload.e[0].Event === 'ViewProduct') {
          // if(payload && payload.e && payload.e.length > 0 && payload.e[0] && payload.e[0].Event === 'ViewProduct') {
            let info = payload.e[0];
            // let info = payload.ProductInfo;
            console.log('PPP the ProductInfo', info)
            let productInfo = {};
            if (info.Name) {
              productInfo['Name'] = info.Name;
              console.log('[PA Phantom Tracker] Name: ' + info.Name);
            }
            if (info.Price) {
              productInfo['Price'] = info.Price;
              console.log('[PA Phantom Tracker] Price: ' + info.Price);
            }
            if (info.IsInStock) {
              productInfo['IsInStock'] = info.IsInStock;
              console.log('[PA Phantom Tracker] IsInStock:' + info.IsInStock);
            }
            if (info.ImageURL) {
              productInfo['ImageURL'] = info.ImageURL;
              console.log('[PA Phantom Tracker] ImageURL:' + info.ImageURL);
            }
            if (info.RefId) {
              productInfo['RefId'] = info.RefId;
              console.log('[PA Phantom Tracker] RefId:' + info.RefId);
            }
            if (info.SKU) {
              productInfo['SKU'] = info.SKU;
              console.log('[PA Phantom Tracker] SKU:' + info.SKU);
            }
            if (info.Url) {
              productInfo['Url'] = info.Url;
              console.log('[PA Phantom Tracker] Url:' + info.Url);
            }
            if (info.Description) {
              productInfo['Description'] = info.Description;
              console.log('[PA Phantom Tracker] Description:' + info.Description);
            }
            pageObject.setStatus('200');
            pageObject.setProductPage(true);
            pageObject.setProductInfo(productInfo);
            pageObject.setMessage('Is a product page.');
            console.log('PPP All good')
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }
      })
    } else {
      pageObject.setStatus('200');
      pageObject.setMessage('Not a product page.');
    }
    return pageObject;
  }

  async _checkProductPage(page) {
    const queryEle = clientProductPages[this.webisteName]['productPage']
    return await page.evaluate(queryEle => {
      let el = document.querySelector(queryEle);
      if (el) {
        return true;
      }
      return false;
    }, queryEle);
  }

  async isRedirected(response) {
    try {
      const chain = response.request().redirectChain();
      if (chain.length > 0) {
        return true;
      }
      return false;
    } catch(e) {
      console.error('Error - something went wrong isRedirected: ', e.message);
    }
  }
}

module.exports.TestPhantomTracker = TestPhantomTracker;
