const fs = require('fs');

class Writer {
  constructor(websiteName, rawData) {
    this.rawData = rawData;
    this.websiteName = websiteName;
  }

  writingFile() {
    let fileName = this.websiteName + '.json'
    let path = './data' + '/' + fileName
    try {
      let data = JSON.stringify(this.rawData, null, 2);
      fs.writeFileSync( path, data);
      console.log('Successfully! Data have been stored.')
      return true
    } catch(e) {
      console.error('Error - Error happend at writer Object writingFile function: ',e.message)
    }
  }
}

module.exports.Writer = Writer;