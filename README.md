# README #

### [1.Project Design](#project-design) ###
### [2.API Design](#) ###
- [2.1 PhantomJS Side](#)
- [2.2 NAGA Side](#)
### [3.Project Env](#) ###

---
## 1.Project Design ##
![work flow design diagram](img/mermaid-design-1.png)

---
## 2.API Design ##
### 2.1 PhantomJS Side
API call **endpoint**:

```javascript
POST /update
```

API call with body **parameters**:

- websiteName(required): website name with **lower case**. For example, 'habitat'. 

- sitemap(required): the sitemap **XML** file url, which is used to fetch website pages urls.

Possible response status:

- 400: Invalid body data. Please check request data.

- 200: Successful response.

Successful response format:

- status: 200

- message: Successfull get data list

- **data**: All pages tracker results, JSON format, including invalidate pages.

data format:

| Key | Value | |


**Response example:**
```json
{
    "status": "200",
    "message": "Successfull get data list",
    "data": {
         "https://www.habitat.co.uk/karma-green-metal-vase-with-small-handle-780227": {
            "url": "https://www.habitat.co.uk/karma-green-metal-vase-with-small-handle-780227",
            "status": "200",
            "message": "Is a product page.",
            "isProductPage": true,
            "productInfo": {
                "Name": "KARMA Green metal objet with small handle",
                "ImageURL": [
                    "https://cdn.habitat.co.uk/media/catalog/product/cache/1/image/315x238/9df78eab33525d08d6e5fb8d27136e95/7/8/780227.jpg",
                    "https://cdn.habitat.co.uk/media/catalog/product/cache/1/image/315x238/9df78eab33525d08d6e5fb8d27136e95/7/8/780227_1.jpg"
                ],
                "RefId": "23591",
                "SKU": "780227",
                "Url": "https://www.habitat.co.uk/karma-green-metal-vase-with-small-handle-780227"
            }
        },
         "https://www.habitat.co.uk/lighting": {
            "url": "https://www.habitat.co.uk/lighting",
            "status": "200",
            "message": "Not a product page.",
            "isProductPage": false,
            "productInfo": {}
        },
         "https://www.habitat.co.uk/sofas-armchairs": {
            "url": "https://www.habitat.co.uk/sofas-armchairs",
            "status": "200",
            "message": "Not a product page.",
            "isProductPage": false,
            "productInfo": {}
        }
    }
}
```

### 2.2 Naga Side
---

## 3.Project Env ##

Express
NPM : https://docs.npmjs.com/cli/install

I would suggest you install nodemon, as it will automatically refresh the terminal if you code changed:
https://github.com/remy/nodemon#nodemon
npm install -g nodemon
npm install --save-dev nodemon(I am consider add it to dev)

>>>
npm install all the defult dependency packages
```shell
npm install
```
>>>


>>>
npm install all the developemnt dependency packages
```shell
npm install
```
>>>

>>>
check all the package installed in current project
```shell
 npm list --depth=0 
```
>>>

use *nodemon app.js* replace *node app.js* to start express server.

test xml path: **'~/Documents/Git/pa.phantomjs.scrape/test.xml'**
