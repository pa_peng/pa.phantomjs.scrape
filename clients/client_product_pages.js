const clientProductPages = {
  habitat: {
    productPage: 'meta[content="product"]',
    product: 'article.product-view',
  }
};

exports.clientProductPages = clientProductPages;