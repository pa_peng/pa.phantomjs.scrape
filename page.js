class Page {
  constructor(url) {
    this._url = url;
    this._status = '';
    this._message = '';
    this._isProductPage = false;
    this._productInfo = {};
  }

  getUrl() {
    return this._url;
  }

  setStatus(status) {
    this._status = status;
  }

  getStatus() {
    return this._status;
  }

  setMessage(message) {
    this._message = message;
  }

  getMessage() {
    return this._message;
  }

  setProductPage(isProductPage) {
    this._isProductPage = isProductPage;
  }

  getProductPage() {
    return this._isProductPage;
  }

  setProductInfo(productInfo) {
    this._productInfo = productInfo;
  }

  getProductInfo() {
    return this._productInfo;
  }

  getJsonSummary() {
    return {
      url: this._url,
      status: this._status,
      message: this._message,
      isProductPage: this._isProductPage,
      productInfo: this._productInfo,
    }
  }
}

module.exports.Page = Page;