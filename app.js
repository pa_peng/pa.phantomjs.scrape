const express = require('express');
const bodyParser = require('body-parser');
const { PhantomTracker } = require('./phantom_tracker');
const { TestPhantomTracker } = require('./test_phantom_tracker');
const { Writer } = require('./write_file');
const { parseSitemap } = require('./sitemap');
const app = express();
const port = 3000;

// var timeout = express.timeout
// app.use(timeout(120000));

app.listen(port, () => console.log(`--------------------SERVER INITIALIZED ON PORT ${port}!--------------------`))

// parse various different custom JSON types as JSON
app.use(bodyParser.json())

app.get('/', (req, res) => res.send('Welcome to the Phantom Tracker!'))

app.post('/Scan', async function (request, response) {
  console.log('API triggered /Scan')
  console.log('request.body', request.body)
  if (request.body && request.body.data && request.body.data.length > 0) {
    const phantomTracker = new PhantomTracker(request.body.data);
    const res = await phantomTracker.run();
    response.send(res);
  } else {
    response.send({
      status: '400',
      message: 'Invalid body data. Format: { data: ["url1", "url2"] }'
    })
  }
});

app.post('/Test', async function (request, response) {
  console.log('API triggered /Test')
  console.log('request.body', request.body)
  if (request.body && request.body.sitemap) {
    const websiteName = 'habitat'
    let urls = await parseSitemap(request.body.sitemap, true, 20);  //for xml which includes the url, we paras it from xml first.
    urls = urls.map(x => x.trim());
    const testPhantomTracker = new TestPhantomTracker(urls, websiteName);
    const data = await testPhantomTracker.run();
    console.log('All data have collected already here,', data);

    const writer = new Writer(websiteName, data);
    writer.writingFile();

    // response.send(data);  
  } else {
    response.send({
      status: '400',
      message: 'Invalid body data. Need XML sitemap.'
    })
  }
});

//https://padevdatalake.blob.core.windows.net/dev-webserver/test.xml
app.post('/Update', async function(req, res){
  if (req.body && req.body.sitemap && req.body.websiteName){
    const websiteName = req.body.websiteName;
    let urls = await parseSitemap(req.body.sitemap, false);
    // let urls = await parseSitemap(req.body.sitemap, true, 20)
    urls = urls.map(x => x.trim());
    const phantomTracker = new PhantomTracker(urls, websiteName);
    const data = await phantomTracker.run();
    const writer = new Writer(websiteName, data);
    writer.writingFile();
    //finally return the result
    // res.send({
    //   status: '200',
    //   message: 'Successfully get data list',
    //   data: data,
    //   check: {
    //     "Name": websiteName,
    //     "URLS": urls.length
    //   }
    // })
  } else {
    res.send({
      status: '400',
      message: 'Invalid body data. Please check request data.'
    })
  }
})