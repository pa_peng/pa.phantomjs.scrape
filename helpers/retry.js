function retry(func, timeout, maxAttempt, exponentialBackOff = false) {
  return new Promise((resolve, reject) => {
    let attempt = 0
    let previousStep = 0
    let currentStep = 1

    const retry = async () => {
      let newTimeout = timeout
      if (exponentialBackOff) {
        let temp = currentStep
        currentStep = currentStep + previousStep // calculate current step by fibonacii rule
        previousStep = temp
        newTimeout = timeout * currentStep
      }
      attempt++

      try {
        resolve(await func())
      } catch (err) {
        if (attempt < maxAttempt) {
          setTimeout(retry, newTimeout)
        } else {
          reject(err)
        }
      }
    }
    retry()
  })
}

module.exports.retry = retry;
